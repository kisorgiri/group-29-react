// redux

// web architecture
// MVC (seperation of concerns)
// Models,Views,Controller
// data flow is bidirection between model and controller

// state management tool/architecture
// FLUX architecture
// Views, Actions, Dispatchers , Store

// unidirection data flow
// application logic resides on store
// there can be multiple store

// REDUX
// state management tool
// Views , Actions , Reducer , Store

// unidirectional data flow
// application logic reside on reducer
// there is only single store
// single source of truth

// pre-requisities
// js project

// dev pacakges
// redux ==> library for state manangement
// react-redux ==> glue to connect react application with redux
// middleware ===> delay (redux-thunk) 