import { toast } from 'react-toastify';

const showSuccess = (msg) => {
    toast.success(msg);
}

const showInfo = (msg) => {
    toast.info(msg);
}

const showWarning = (msg) => {
    toast.warning(msg);
}

const showError = (msg) => {
    toast.error(msg);
}

const handleError = (err) => {
    debugger;
    let errMsg = 'something went wrong';
    const error = err && err.response;
    if (error && error.data) {
        // if msg
        errMsg = error.data.msg;
    }
    // steps to follow
    // 1. check err
    // 2. parse parse
    // 3. extract error message
    // 4.prepare error message
    // 5.show them in UI
    showError(errMsg);
}

export default {
    showSuccess,
    showInfo,
    showWarning,
    handleError
}

