import moment from 'moment';

export const formatDate = (date, format = 'DD/MM/YYYY') => {
    if (!date) return null;
    return moment(date).format(format);
}

export const relativeTime = (date) => {
    if (!date) return null;
    return moment(date).startOf('minute').fromNow();
}