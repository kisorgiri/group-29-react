import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BASE_URL;


const getHeaders = (secured) => {
    let options = {
        'Content-Type': 'application/json'
    }
    if (secured) {
        options['Authorization'] = localStorage.getItem('token');
    }
    return options
}
const http = axios.create({
    baseURL: BASE_URL,
    responseType: 'json'
})

const POST = (url, data, isSecure = false, params = {}) => {
    return http.post(url, data, {
        headers: getHeaders(isSecure),
        params
    })
}
const GET = (url, isSecure = false, params = {}) => {
    return http.get(url, {
        headers: getHeaders(isSecure),
        params
    })
}
const PUT = (url, data, isSecure = false, params = {}) => {
    return http.put(url, data, {
        headers: getHeaders(isSecure),
        params
    })
}

const DELETE = (url, isSecure = false, params = {}) => {
    return http.delete(url, {
        headers: getHeaders(isSecure),
        params
    })
}

const UPLOAD = (method, url, data, files = []) => {
    // file upload through xhr
    return new Promise((resolve, reject) => {
        const formData = new FormData();
        const xhr = new XMLHttpRequest();

        // add file in formdata
        if (files.length) {
            files.forEach(file => {
                formData.append('img', file, file.name);
            })
        }

        // add textual data in formdata
        for (let key in data) {
            formData.append([key], data[key]);
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response);
                }
                else {
                    reject(xhr.response);
                }
            }
        }

        xhr.open(method, `${BASE_URL}${url}?token=${localStorage.getItem('token')}`, true);
        xhr.send(formData)
    })


}

export default {
    GET,
    POST,
    PUT,
    DELETE,
    UPLOAD
}