// main file to load conenent in index.html
import React from 'react';
import ReactDOM from 'react-dom';

import { App } from './components/app.component'

const mainDiv = document.getElementById('root');

ReactDOM.render(<App address="bkt" phone="something" />, mainDiv)