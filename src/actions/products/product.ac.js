import httpClient from "../../util/httpClient";
import notify from "../../util/notify";
import { PRODUCT_RECEIVED, PRODUCT_REMOVED, SET_IS_LOADING } from "./type"

//     }
// }


// function fetch_product(params){

//     return function(dispatch){
//         // dispatch actions to reducer
//     }
// }

export const fetch_product_ac = params => dispatch => {
    console.log('here at action');
    dispatch(isLoading(true))
    httpClient.GET('/product', true)
        .then(response => {
            dispatch({
                type: PRODUCT_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => {
            notify.handleError(err);
        })
        .finally(() => {
            dispatch(isLoading(false))
        })
}

export const remove_product_ac = (id) => (dispatch) => {

    httpClient
        .DELETE(`/product/${id}`, true)
        .then(response => {
            notify.showInfo('Product Removed');
            dispatch({
                type: PRODUCT_REMOVED,
                payload: id
            })
        })
        .catch(err => {
            notify.handleError(err);
        })
}

const isLoading = loading => ({
    type: SET_IS_LOADING,
    payload: loading
})