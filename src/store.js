import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';
import thunk from 'redux-thunk';

const initialState = {};

const middlewares = [thunk];
export const store = createStore(rootReducer, initialState, applyMiddleware(...middlewares));