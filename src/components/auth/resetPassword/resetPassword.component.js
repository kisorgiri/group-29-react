import React, { Component } from 'react';
import { Button } from '../../common/button/button.component';
import { Link } from 'react-router-dom';
import notify from './../../../util/notify';
import httpClient from '../../../util/httpClient';

const defaultForm = {
    password: '',
    confirmPassword: '',
}
export class ResetPassword extends Component {
    constructor() {
        super();// parent class constructor call
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false,
        };
    }
    componentDidMount() {
        this.id = this.props.match.params['id'];
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        httpClient
            .POST(`/auth/reset-password/${this.id}`, this.state.data)
            .then(response => {
                notify.showInfo("Password reset successfull please login")
                this.props.history.push('/');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    handleChange = (e) => {
        const { name, value } = e.target;

        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm(fieldName) {
        let errMsg;

        switch (fieldName) {
            case 'password':
                errMsg = this.state.data['confirmPassword']
                    ? this.state.data[fieldName] === this.state.data['confirmPassword']
                        ? ''
                        : 'password didnot match'
                    : this.state.data[fieldName]
                        ? this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                        : 'required field*'
                break;
            case 'confirmPassword':
                errMsg = this.state.data['password']
                    ? this.state.data[fieldName] === this.state.data['password']
                        ? ''
                        : 'password didnot match'
                    : this.state.data[fieldName]
                        ? this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                        : 'required field*'
                break;

            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            let errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    render() {

        return (
            <div>
                <h2>Reset Password</h2>
                <p>Please Choose your password</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" name="password" id="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>
                    <label htmlFor="confirmPassword">Confirm Password</label>
                    <input className="form-control" type="password" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.confirmPassword}</p>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    >
                    </Button>
                </form>
            </div>
        )
    }
}