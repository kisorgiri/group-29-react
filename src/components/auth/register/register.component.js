import React, { Component } from 'react';
import { Button } from '../../common/button/button.component';
import { Link } from 'react-router-dom';
import notify from './../../../util/notify';
import httpClient from './../../../util/httpClient';

const defaultForm = {
    name: '',
    username: '',
    password: '',
    confirmPassword: '',
    email: '',
    phone: '',
    dob: '',
    gender: '',
    temp_adderss: '',
    permanent_address: ''
}

export class Register extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false
        };
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState((preState) => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name)
        })
    }

    validateForm = (fieldName) => {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field*'
                break;
            case 'password':
                errMsg = this.state.data['confirmPassword']
                    ? this.state.data[fieldName] === this.state.data['confirmPassword']
                        ? ''
                        : 'password didnot match'
                    : this.state.data[fieldName]
                        ? this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                        : 'required field*'
                break;
            case 'confirmPassword':
                errMsg = this.state.data['password']
                    ? this.state.data[fieldName] === this.state.data['password']
                        ? ''
                        : 'password didnot match'
                    : this.state.data[fieldName]
                        ? this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                        : 'required field*'
                break;
            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'invalid email'
                    : 'required field*'
                break;

            default:
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })

        })

    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        // http call
        httpClient
            .POST(`/auth/register`, this.state.data)
            .then(response => {
                notify.showSuccess('Registration Successfull please login');
                this.props.history.push('/');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })


    }

    render() {
        const { isSubmitting, isValidForm } = this.state;
        return (
            <div>
                <h2>Register</h2>
                <p>Please provide your details to continue</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input name="name" className="form-control" type="text" placeholder="Name" onChange={this.handleChange}></input>
                    <label>Phone Number</label>
                    <input name="phoneNumber" className="form-control" type="number" placeholder="Phone Number" onChange={this.handleChange}></input>
                    <label>Username</label>
                    <input name="username" className="form-control" type="text" placeholder="Username" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username}</p>
                    <label>Password</label>
                    <input name="password" className="form-control" type="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>

                    <label>Confirm Password</label>
                    <input name="confirmPassword" className="form-control" type="password" placeholder="Confirm Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.confirmPassword}</p>

                    <label>Email</label>
                    <input name="email" className="form-control" type="text" placeholder="Email" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>

                    <label>Gender</label>
                    <input name="gender" className="form-control" type="text" placeholder="Gender" onChange={this.handleChange}></input>
                    <label>D.O.B</label>
                    <input name="dob" className="form-control" type="date" onChange={this.handleChange}></input>
                    <label>Temporary Address</label>
                    <input name="temp_address" className="form-control" type="text" placeholder="Temporary Address" onChange={this.handleChange}></input>
                    <label>Permanent Address</label>
                    <input name="permanent_Address" className="form-control" type="text" placeholder="Permanent Address" onChange={this.handleChange}></input>
                    <br />
                    <Button
                        isSubmitting={isSubmitting}
                        isValidForm={isValidForm}
                    ></Button>

                </form>
                <p>Already Registered?</p>
                <p>back to <Link to="/">login</Link></p>
            </div>
        )
    }
}
