import React, { Component } from 'react';
import { Button } from '../../common/button/button.component';
import { Link } from 'react-router-dom';
import notify from './../../../util/notify';
import httpClient from '../../../util/httpClient';

const defaultForm = {
    username: '',
    password: '',
}
export class Login extends Component {
    constructor() {
        super();// parent class constructor call
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false,
            remember_me: false,
        };// this is an object to hold internal data of a component
    }
    componentDidMount() {
        if (localStorage.getItem('remember_me')) {
            this.props.history.push('/dashboard/random');
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        httpClient
            .POST(`/auth/login`, this.state.data)
            .then(response => {
                console.log('response >>', response);
                notify.showSuccess(`Welcome ${response.data.user.username}`);
                // web storage
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user));
                localStorage.setItem('remember_me', this.state.remember_me);

                this.props.history.push('/dashboard');


            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })

            })


    }

    handleChange = (e) => {
        const { name, value, type, checked } = e.target;
        if (type === 'checkbox') {
            localStorage.setItem('remember_me', checked);

            return this.setState({
                remember_me: checked
            })
        }
        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm(fieldName) {
        let errMsg = this.state.data[fieldName]
            ? ''
            : `${fieldName} is required`;

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            let errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    render() {
        // render method is mandatory in class based component
        // render method must return a single html node
        // UI logic goes inside render
        return (
            <div>
                <h2>Login</h2>
                <p>Please login to start your session</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" name="username" id="username" placeholder="Username" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" name="password" id="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>
                    <input type="checkbox" onChange={this.handleChange} name="remember_me" id="remember_me" ></input>
                    <label htmlFor="remember_me">Remeber Me</label>

                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                        enabledLabel="Login"
                        disabledLabel="Logingin..."
                    >
                    </Button>
                </form>
                <p>Don't Have an account?</p>
                <p style={{ float: 'left' }} >Register <Link to="/register">here</Link></p>
                <p style={{ float: 'right' }}> <Link to="/forgot_password">forgot password?</Link></p>
            </div>
        )
    }
}