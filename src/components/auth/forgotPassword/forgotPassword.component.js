import React, { Component } from 'react'
import httpClient from '../../../util/httpClient'
import notify from '../../../util/notify'
import { Button } from '../../common/button/button.component'

export default class ForgotPassword extends Component {
    constructor() {
        super()

        this.state = {
            email: '',
            emailErr: '',
            isValidForm: false,
            isSubmitting: false
        }
    }

    handleSubmit = e => {
        const { email } = this.state;
        this.setState({
            isSubmitting: true
        })
        e.preventDefault();
        httpClient.POST('/auth/forgot-password', { email })
            .then(response => {
                notify.showInfo("Password reset link sent to your email please check inobx");
                this.props.history.push('/');
            })
            .catch((err) => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }
    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        }, () => {
            this.validateForm(name);
        })
    }

    validateForm = fieldName => {
        let errMsg = this.state[fieldName]
            ? this.state[fieldName].includes('@') && this.state[fieldName].includes('.com')
                ? ''
                : 'invalid email address'
            : 'required field';
        this.setState({
            emailErr: errMsg
        }, () => {
            this.setState({
                isValidForm: !this.state.emailErr
            })
        })
    }

    render() {
        return (
            <>
                <h2>Forgot Password</h2>
                <p>Please provide your registered email to reset your password</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Email</label>
                    <input className="form-control" type="text" placeholder="Email Address" name="email" onChange={this.handleChange}></input>
                    <p className="error">{this.state.emailErr}</p>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}></Button>
                </form>

            </>
        )
    }
}
