import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { AddProduct } from './product/addProduct/addProduct.component';
import { EditProduct } from './product/editProduct/editProduct.component';
import { ViewProduct } from './product/viewProduct/viewProduct.component';
import { Login } from './auth/login/login.component';
import { Register } from './auth/register/register.component';
import { NavBar } from './common/navbar/navbar.component';
import { Sidebar } from './common/sidebar/sidebar.component';
import { SearchProduct } from './product/searchProduct/searchProduct.component';
import ForgotPassword from './auth/forgotPassword/forgotPassword.component';
import { ResetPassword } from './auth/resetPassword/resetPassword.component';
import { ChatComponent } from './user/message/message.component';

const Home = (props) => {
    return <p>Home Page</p>
}

const Dashboard = (props) => {
    console.log('props >>', props)
    return <p>Welcome to Our Store please use side navigation menu or contact system administrator for support</p>
}

const NotFound = () => {
    return (
        <div>
            <p>Not Found</p>
            <img src="./images/download.png" alt="notfound.png" width="400px"></img>
        </div>
    )
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={routeProps => (
        localStorage.getItem('token')
            ? <>
                <div>
                    <NavBar isLoggedIn={true}></NavBar>
                </div>
                <div>
                    <Sidebar></Sidebar>
                </div>
                <div className="main">
                    <Component {...routeProps}></Component>
                </div>
            </>
            : <Redirect to="/"></Redirect>
    )}></Route>
}

const PublicRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={routeProps => (
        <>
            <div>
                <NavBar isLoggedIn={localStorage.getItem('token') ? true : false}></NavBar>
            </div>
            <div className="main">
                <Component {...routeProps}></Component>
            </div>
        </>
    )}></Route>
}

export const AppRouting = (props) => {
    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute exact path="/" component={Login}></PublicRoute>
                <PublicRoute path="/register" component={Register} ></PublicRoute>
                <PublicRoute path="/home" component={Home}></PublicRoute>

                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct}></ProtectedRoute>
                <ProtectedRoute path="/view_product" component={ViewProduct}></ProtectedRoute>
                <ProtectedRoute path="/search_product" component={SearchProduct}></ProtectedRoute>
                <ProtectedRoute path="/message" component={ChatComponent}></ProtectedRoute>
                <PublicRoute path="/forgot_password" component={ForgotPassword}></PublicRoute>
                <PublicRoute path="/reset_password/:id" component={ResetPassword}></PublicRoute>
                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>

        </BrowserRouter>
    )
}


// library == >react router dome
// BrowserRouter ==> wrapper
// Route ==> configuration block
// dynamic handler
// Switch==> if valid route meets it will not load another
// Link ==> instead of anchor tag we use Link to navigate on users click
// note Link cannot be used outside browserRouter context environmanet

// props
// history ==> use to navigate using function
// match ==> consume dynamic url value
// location ==> consume optional query url value