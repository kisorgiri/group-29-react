import React, { Component } from 'react'
import * as io from 'socket.io-client';
import { relativeTime } from '../../../util/date.processing';
import notify from '../../../util/notify';
import './message.component.css';

const socketURL = process.env.REACT_APP_SOCKET_URL;

export class ChatComponent extends Component {
    constructor() {
        super()

        this.state = {
            msgBody: {
                message: '',
                senderId: '',
                receiverId: '',
                receiverName: '',
                senderName: '',
                time: ''
            },
            messages: [],
            users: []
        }
    }

    componentDidMount() {
        this.socket = io(socketURL);
        this.user = JSON.parse(localStorage.getItem('user'));
        this.runSocket();
    }

    runSocket() {
        this.socket.emit('new-user', this.user.username);
        this.socket.on('reply-msg', (data) => {
            const { messages, msgBody } = this.state;
            // swap sender to receiver
            msgBody.receiverId = data.senderId;
            msgBody.receiverName = data.senderName;
            messages.push(data);
            this.setState({
                messages,
                msgBody: msgBody
            })
        })
        this.socket.on('reply-msg-own', (data) => {
            const { messages } = this.state;

            messages.push(data);
            this.setState({
                messages,
            })
        })
        this.socket.on('users', (users) => {
            this.setState({
                users: users
            })
        })

    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(pre => ({
            msgBody: {
                ...pre.msgBody,
                [name]: value
            }
        }))
    }

    send = (e) => {
        e.preventDefault();
        const { msgBody, users } = this.state;
        // prepare message
        // add time add sender
        if (!msgBody.receiverId) {
            return notify.showInfo("Please select a user to continue")
        }
        const sender = users.find((user, index) => user.name === this.user.username);
        msgBody.senderId = sender.id;
        msgBody.time = Date.now();
        msgBody.senderName = this.user.username;
        this.socket.emit('new-msg', msgBody);
        this.setState(pre => ({
            msgBody: {
                ...pre.msgBody,
                message: ''
            }
        }))
    }

    selectUser = (user) => {
        this.setState(pre => ({
            msgBody: {
                ...pre.msgBody,
                receiverId: user.id,
                receiverName: user.name
            }
        }))
    }

    render() {
        return (
            <>
                <h2>Messages</h2>
                <div className="row">
                    <div className="col-md-6">
                        <ins>Messages</ins>
                        <div className="chat_box">
                            {this.state.messages.map((msg, index) => (
                                <div key={index}>
                                    <h3>{msg.message}</h3>
                                    <p>{msg.senderName}</p>
                                    <small>{relativeTime(msg.time)}</small>
                                </div>
                            ))}
                        </div>
                        <form className="form-group" onSubmit={this.send} noValidate>
                            <input className="form-control" value={this.state.msgBody.message} type="text" placeholder="Your message here" name="message" onChange={this.handleChange}></input>
                            <button type="submit" className="btn btn-success">send</button>
                        </form>
                    </div>
                    <div className="col-md-6">
                        <ins>Users</ins>
                        <div className="chat_box">
                            {this.state.users.map((user, index) => (
                                <button style={{ display: 'block' }} key={index} className="btn btn-default" onClick={() => this.selectUser(user)}>{user.name}</button>
                            ))}

                        </div>
                    </div>
                </div>
            </>
        )
    }
}
