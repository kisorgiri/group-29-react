import React from 'react'; // not mandatory in react >17
import { AppRouting } from './app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import provider
import { Provider } from 'react-redux';
import { store } from '../store';


export const App = (props) => { //named export
    return (
        <div>
            <ToastContainer />
            <Provider store={store}>
                <AppRouting />
            </Provider>

        </div>

    )
}


// Component 
// component is basic building block of react which always returns a single html node
// component holds it's own behaviour
// we can pass data and manage data within a component
// component can be written in two ways
// functional component and class based component
// stateless component and statefull component

// throughout this course 
// statefull component ===> class based component
// stateless component====> functional component

// react 16.8 > we can maintain statefull component in functional component as well(using HOOKS)

// props and state
// props are incoming data inside a component

// state
// data within a component