import React, { Component } from 'react'
import { Button } from './../../common/button/button.component'

const IMG_URL = process.env.REACT_APP_IMG_URL;

const defaultForm = {
    name: '',
    category: '',
    price: '',
    color: '',
    brand: '',
    manuDate: '',
    expiryDate: '',
    tags: '',
    offers: '',
    discountedItem: '',
    discountType: '',
    discountValue: '',
    warrentyStatus: '',
    warrentyPeroid: '',
    description: ''

}
export class ProductForm extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isValidForm: false,
            filesToUpload: []
        }
    }

    componentDidMount() {
        if (this.props.productData) {
            this.setState({
                data: {
                    ...defaultForm,
                    ...this.props.productData,
                    discountedItem: this.props.productData.discount ? this.props.productData.discount.discountedItem : '',
                    discountType: this.props.productData.discount ? this.props.productData.discount.discountType : '',
                    discountValue: this.props.productData.discount ? this.props.productData.discount.discountValue : ''

                }
            })
        }
    }

    handleChange = e => {
        let { name, value, checked, type, files } = e.target;

        if (type === 'file') {
            const { filesToUpload } = this.state;
            filesToUpload.push(files[0]);
            return this.setState({
                filesToUpload
            })
        }
        if (type === 'checkbox') {
            value = checked;
        }
        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm = fieldName => {

        let errMsg;
        switch (fieldName) {
            case 'brand':
            case 'name':
            case 'category':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field*'
                break;
            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })

    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.submitCallback(this.state.data, this.state.filesToUpload)
    }

    render() {
        const discountContent = this.state.data.discountedItem
            ? <>
                <label>Discount Type</label>
                <input type="text" name="discountType" value={this.state.data.discountType} placeholder="Discount Type" className="form-control" onChange={this.handleChange}></input>
                <label>Discount Value</label>
                <input type="text" name="discountValue" value={this.state.data.discountValue} placeholder="Discount Value" className="form-control" onChange={this.handleChange}></input>
            </>
            : ''
        return (
            <>
                <h2>{this.props.title}</h2>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Name" value={this.state.data.name} className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.name}</p>
                    <label>Description</label>
                    <input type="text" name="description" placeholder="Description" value={this.state.data.description} className="form-control" onChange={this.handleChange}></input>
                    <label>Category</label>
                    <input type="text" name="category" placeholder="Category" value={this.state.data.category} className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.category}</p>

                    <label>Brand</label>
                    <input type="text" name="brand" placeholder="Brand" value={this.state.data.brand} className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.brand}</p>

                    <label>Price</label>
                    <input type="text" name="price" placeholder="Price" value={this.state.data.price} className="form-control" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input type="text" name="color" placeholder="Color" value={this.state.data.color} className="form-control" onChange={this.handleChange}></input>
                    <label>Manu Date</label>
                    <input type="date" name="manuDate" value={this.state.data.manuDate} className="form-control" onChange={this.handleChange}></input>
                    <label>Expiry Date</label>
                    <input type="date" name="expiryDate" value={this.state.data.expiryDate} className="form-control" onChange={this.handleChange}></input>
                    <label>Tags</label>
                    <input type="text" name="tags" placeholder="Tags" value={this.state.data.tags} className="form-control" onChange={this.handleChange}></input>
                    <label>Offers</label>
                    <input type="text" name="offers" value={this.state.data.offers} placeholder="Offers" className="form-control" onChange={this.handleChange}></input>
                    <br />
                    <input style={{ marginRight: '5px' }} type="checkbox" checked={this.state.data.warrentyStatus} name="warrentyStatus" onChange={this.handleChange}></input>
                    <label>Warrenty Item</label>
                    <br />
                    {
                        this.state.data.warrentyStatus && (
                            <>
                                <label>Warrenty Peroid</label>
                                <input type="text" name="warrentyPeroid" value={this.state.data.warrentyPeroid} placeholder="Warrenty Peroid" className="form-control" onChange={this.handleChange}></input>
                            </>
                        )
                    }
                    <input type="checkbox" name="discountedItem" checked={this.state.data.discountedItem} onChange={this.handleChange}></input>
                    <label>Discounted Item</label>
                    <br />
                    {discountContent}
                    {
                        this.props.productData && this.props.productData.images && this.props.productData.images.length > 0 && (
                            <>
                                <label>Previous Image</label>
                                <br />
                                <img src={`${IMG_URL}/${this.props.productData.images[0]}`} width="400px" alt="product_image.png"></img>
                                <br />
                            </>

                        )
                    }

                    <label>Choose Image</label>
                    <br />
                    <input type="file" onChange={this.handleChange}></input>
                    <br />
                    <br />
                    <Button
                        isValidForm={this.state.isValidForm}
                        isSubmitting={this.props.isSubmitting}
                    >

                    </Button>
                </form>
            </>
        )
    }
}
