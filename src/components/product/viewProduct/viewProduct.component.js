import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { formatDate } from '../../../util/date.processing';
import { connect } from 'react-redux';
import { fetch_product_ac, remove_product_ac } from './../../../actions/products/product.ac';
import { Loader } from '../../common/loader/loader.component';

const IMG_URL = process.env.REACT_APP_IMG_URL;

class ViewProductProduct extends Component {
    constructor() {
        super()
    }
    componentDidMount() {
        console.log('at init >>', this.props)
        this.props.fetch();
        if (this.props.searchData) {
            this.setState({
                products: this.props.searchData
            })

        }
    }

    editProduct = (id) => {
        this.props.history.push(`/edit_product/${id}`);
    }

    removeProduct = (id, index) => {
        // ask confirmation
        const confirmation = window.confirm('Are you sure to remove?');
        if (confirmation) {
            this.props.remove_product(id);
        }
    }

    render() {
        let mainContent = this.props.isLoading
            ? <Loader></Loader>
            : <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Brand</th>
                        <th>Price</th>
                        <th>Created At</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.products.map((product, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td> <Link to={`/product_details/${product._id}`}>{product.name}</Link> </td>
                            <td>{product.category}</td>
                            <td>{product.brand}</td>
                            <td>{product.price}</td>
                            <td>{formatDate(product.createdAt)}</td>
                            <td>
                                <img src={`${IMG_URL}/${product.images[0]}`} width="200px" alt="product_image.png"></img>
                            </td>
                            <td>
                                <button onClick={() => this.editProduct(product._id)} className="btn btn-info">edit</button>
                                <button onClick={() => this.removeProduct(product._id, index)} className="btn btn-danger">delete</button>
                            </td>
                        </tr>
                    ))}


                </tbody>
            </table>
        return (
            <>
                <h2>View Products</h2>
                {this.props.searchData && (<button className="btn btn-success" onClick={this.props.searchAgain}>search again</button>)}

                {mainContent}
            </>
        )
    }
}

// mapStateToProps
// what comes in from store
const mapStateToProps = store => ({
    isLoading: store.product.isLoading,
    products: store.product.products
})

// mapDispatchToProps
const mapDispatchToProps = dispatch => ({
    fetch: () => dispatch(fetch_product_ac()),
    remove_product: (id) => dispatch(remove_product_ac(id))
})


export const ViewProduct = connect(mapStateToProps, mapDispatchToProps)(ViewProductProduct);