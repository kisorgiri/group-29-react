import React, { Component } from 'react'
import { ProductForm } from '../productForm/productForm.component';
import httpClient from './../../../util/httpClient';
import notify from './../../../util/notify';

export class AddProduct extends Component {
    constructor() {
        super()

        this.state = {
            isSubmitting: false
        }
    }
    add = (data, files) => {
        // this.setState({
        //     isSubmitting: true
        // })
        // httpClient.POST('/product', data, true)
        httpClient.UPLOAD('POST', '/product', data, files)
            .then(response => {
                notify.showInfo('Product Added Successfully');
                this.props.history.push('/view_product');
            })
            .catch(err => {
                this.setState({
                    isSubmitting: false
                })
                notify.handleError(err);
            })
    }

    render() {
        return <ProductForm
            title="Add Product"
            submitCallback={this.add}
            isSubmitting={this.state.isSubmitting}
        ></ProductForm>

    }
}
