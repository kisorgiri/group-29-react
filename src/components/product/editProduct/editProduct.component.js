import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notify from '../../../util/notify';
import { Loader } from '../../common/loader/loader.component';
import { ProductForm } from '../productForm/productForm.component';

export class EditProduct extends Component {
    constructor() {
        super()

        this.state = {
            isLoading: false,
            product: {}
        }
    }

    componentDidMount() {
        const productId = this.props.match.params['id'];
        this.setState({
            isLoading: true
        })
        httpClient.GET(`/product/${productId}`, true)
            .then(response => {
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    edit = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        // todo
        // httpClient.PUT(`/product/${data._id}`, data, true)
        httpClient.UPLOAD('PUT', `/product/${data._id}`, data, files)
            .then(response => {
                notify.showInfo('Product Updated');
                this.props.history.push('/view_product')
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            });
    }

    render() {
        const content = this.state.isLoading
            ? <Loader></Loader>
            : <ProductForm
                title="Edit Product"
                submitCallback={this.edit}
                isSubmitting={this.state.isSubmitting}
                productData={this.state.product}
            ></ProductForm>
        return content;
    }
}

