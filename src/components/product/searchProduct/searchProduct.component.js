import React, { Component } from 'react'
import { Button } from './../../common/button/button.component'
import httpClient from './../../../util/httpClient';
import notify from './../../../util/notify';
import { ViewProduct } from './../viewProduct/viewProduct.component';

const defaultForm = {
    name: '',
    category: '',
    brand: '',
    minPrice: '',
    maxPrice: '',
    multipleDateRange: '',
    fromDate: '',
    toDate: '',
    tags: ''
}

export class SearchProduct extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                ...defaultForm
            },
            allProducts: [],
            categories: [],
            categoryWiseProduct: [],
            error: {
                ...defaultForm
            },
            searchResults: [],
            isSubmitting: false,
            isValidForm: false
        }
    }

    componentDidMount() {
        httpClient.POST('/product/search', {})
            .then(response => {
                let categories = [];
                response.data.forEach((item, index) => {
                    if (categories.indexOf(item.category) === -1) {
                        categories.push(item.category)
                    }
                })
                this.setState({
                    allProducts: response.data,
                    categories
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
    }

    handleChange = (e) => {
        let { name, value, type, checked } = e.target;
        if (name === 'category') {
            this.prepareNameForCategory(value);
        }

        if (type === 'checkbox') {
            value = checked;
        }

        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })

    }
    validateForm = fieldName => {
        let errMsg;
        switch (fieldName) {
            case 'category':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field'
                break;

            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })

        })
    }

    prepareNameForCategory = (category) => {
        const { allProducts } = this.state;
        const categoryWiseProduct = allProducts.filter(item => item.category === category);
        this.setState({
            categoryWiseProduct
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { data } = this.state;
        if (data.fromDate && !data.toDate) {
            data.toDate = data.fromDate
        }
        httpClient.POST('/product/search', data)
            .then(response => {
                if (!response.data.length) {
                    return notify.showInfo("No any product matched your search query");
                }
                this.setState({
                    searchResults: response.data
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
    }


    resetSearch = () => {
        this.setState({
            searchResults: [],
            data: { ...defaultForm }
        })
    }

    render() {
        let content = this.state.searchResults && this.state.searchResults.length
            ? <ViewProduct searchData={this.state.searchResults} searchAgain={this.resetSearch}></ViewProduct>
            : <>
                <h2>Search Product</h2>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Category</label>
                    <select name="category" value={this.state.data.category} className="form-control" onChange={this.handleChange}>
                        <option disabled value="">(Select Category)</option>
                        {this.state.categories.map((cat, i) => (
                            <option key={i} value={cat}>{cat}</option>
                        ))}
                    </select>
                    {
                        this.state.categoryWiseProduct.length > 0 &&
                        (
                            <>
                                <label>Name</label>
                                <select name="name" value={this.state.data.name} className="form-control" onChange={this.handleChange}>
                                    <option disabled value="">(Select Name)</option>
                                    {this.state.categoryWiseProduct.map((product, i) => (
                                        <option key={i} value={product.name}>{product.name}</option>
                                    ))}
                                </select>
                            </>
                        )
                    }
                    <label>Brand</label>
                    <input type="text" className="form-control" name="brand" placeholder="Brand" onChange={this.handleChange} ></input>
                    <label>Color</label>
                    <input type="text" className="form-control" name="color" placeholder="Color" onChange={this.handleChange} ></input>

                    <label>Min Price</label>
                    <input type="number" className="form-control" name="minPrice" placeholder="Min Price" onChange={this.handleChange} ></input>
                    <label>MaxPrice</label>
                    <input type="number" className="form-control" name="maxPrice" placeholder="MaxPrice" onChange={this.handleChange} ></input>
                    <label>Select Date</label>
                    <input type="date" className="form-control" name="fromDate" onChange={this.handleChange} ></input>
                    <input type="checkbox" name="multipleDateRange" onChange={this.handleChange}></input>
                    <label>Multiple Date Range</label>
                    <br />
                    {
                        this.state.data.multipleDateRange && (
                            <>
                                <label>To Date</label>
                                <input type="date" className="form-control" name="toDate" onChange={this.handleChange} ></input>
                            </>
                        )
                    }
                    <label>Tags</label>
                    <input type="text" className="form-control" name="tags" placeholder="Tags" onChange={this.handleChange} ></input>


                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    ></Button>

                </form>
            </>
        return content;
    }
}
