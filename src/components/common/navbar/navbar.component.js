import React from 'react';
import './navbar.component.css'
import { Link, withRouter } from 'react-router-dom';

const logout = (props) => {
    localStorage.clear();
    props.history.push('/')

}

const NavBarComponent = (props) => {
    const user = JSON.parse(localStorage.getItem('user'));
    const menu = props.isLoggedIn
        ? <ul className="nav_list">
            <li className="nav_item">
                <Link to="/dashboard">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/about">About</Link>

            </li>
            <li className="nav_item">
                <Link to="/contacts">Contact</Link>

            </li>
            <li className="nav_item ">
                <button onClick={() => logout(props)} className="btn btn-success logout">logout</button>
                <p className="logout">{user.username}</p>

            </li>
        </ul>
        : <ul className="nav_list">
            <li className="nav_item">
                <Link to="/home">Home</Link>

            </li>
            <li className="nav_item">
                <Link to="/">Login</Link>

            </li>
            <li className="nav_item">
                <Link to="/register">Register</Link>

            </li>
        </ul>
    return (
        <div className="nav_bar">
            {menu}
        </div>
    )
}

export const NavBar = withRouter(NavBarComponent);