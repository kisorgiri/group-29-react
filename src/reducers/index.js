import { combineReducers } from 'redux';
import { productReducer } from './product.red';

export default combineReducers({
    product: productReducer
})

// store
// {productdata:[],
// isLoading:false,
// userdata:[],
// userIsLoading:false,
// userIsSubmitting:false}

// store
// {
//     product: {
//         products: [],
//             isLoading: false,
//                 iSubmitting: false
//     },
//     user: {

//     },
//     auth: {

//     },
//     notification: { }
// }