// js plain function

import { PRODUCT_RECEIVED, PRODUCT_REMOVED, SET_IS_LOADING } from "../actions/products/type";

const intialState = {
    products: [],
    isLoading: false
}

export const productReducer = (state = intialState, action) => {
    console.log('here at reducer >>', action);
    switch (action.type) {
        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case PRODUCT_RECEIVED:
            return {
                ...state,
                products: action.payload
            }

        case PRODUCT_REMOVED:
            const { products } = state;
            products.forEach((product, index) => {
                if (product._id === action.payload) {
                    products.splice(index, 1);
                }
            });
            return {
                ...state,
                products: [...products]
            }

        default:
            return {
                ...state
            };
    }
}